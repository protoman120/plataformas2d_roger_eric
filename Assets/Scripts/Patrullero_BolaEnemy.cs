﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrullero_BolaEnemy : MonoBehaviour
{
    [SerializeField] Transform[] posiciones;
    [SerializeField] int posactual = 0;
    [SerializeField] float velocidadEnemigo = 1.0f;
    [SerializeField] private GameObject graphics;

    private void Update()
    {
        //Debug.Log("Distancia: " + Vector2.Distance(posiciones[posactual].position, transform.position));
        if (Mathf.Abs(Vector2.Distance(posiciones[posactual].position, transform.position)) < 0.1f)
        {
            posactual++;
            graphics.transform.localScale = new Vector3(-1, 1, 1);
                
                      
            if (posactual >= posiciones.Length)
            {
                posactual = 0;
                graphics.transform.localScale = new Vector3(1, 1, 1);
            }
        }

        transform.position = Vector2.MoveTowards(transform.position, posiciones[posactual].position, Time.deltaTime * velocidadEnemigo);
    }
}
