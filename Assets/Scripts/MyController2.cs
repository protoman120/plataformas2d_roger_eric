﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
public class MyController2 : MonoBehaviour
{
    private Rigidbody2D rb2d = null;
    private float move = 0f;
    public float maxS = 11f;
    private bool jump;
    private bool attack;
    public float fuerzaSalto = 5.0f;
    [SerializeField] private GameObject graphics;
    public int PlayerDirection;
    //1. Right / 2. Left
    public int armAnim;

    [SerializeField] private GameObject characterGraphics;
    [SerializeField] private GameObject armGraphics;
    [SerializeField] private GameObject armGraphicsShoot;
    [SerializeField] private GameObject bulletGraphics;
    [SerializeField] private GameObject camera;
    [SerializeField] private Animator animator;
    [SerializeField] private Animator armAnimator;

    public PlayerBullet playerbullet;

    public bool isGrounded = false;
    private bool IsDead = false;
    private bool stopMovement = false;
    private bool IsJumping = false;
    private bool IsShooting = false;

    public AudioSource soundHit;
    public AudioSource soundJump;
    public AudioSource soundShoot;

    private FollowTarget followTarget;

    private float scalaActual;

    Vector3 lookAt;
    Vector3 mivector;
    float AngleRad;
    float AngleDeg;

    private float shootTimer = 0;
    public float cadencia;
    public bool shootReady = true;

    private float deadTimer = 0;
    public float respawnTime;
    public bool isDead = false;

    public BoxCollider2D playerCollision;

    private void Update()
    {
        if (shootReady == false)
        {
            Debug.Log("Calculando tiempo");
            shootTimer += 1 * Time.deltaTime;
            if (shootTimer >= cadencia)
            {
                Debug.Log("Recargando");
                shootTimer = 0;
                shootReady = true;
                armGraphics.SetActive(true);
                armGraphicsShoot.SetActive(false);
                armAnim = 1;
                isGrounded = true;
                if (PlayerDirection == 1)
                {
                    graphics.transform.localScale = new Vector3(-1, 1, 1);
                }else if (PlayerDirection == 2)
                {
                    graphics.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }

        if (isDead == true)
        {
            Debug.Log("Resucitando");
            deadTimer += 1 * Time.deltaTime;
            if (deadTimer >= respawnTime)
            {
                characterGraphics.transform.position = new Vector3(-70.901f, 0.719f, 0);
                deadTimer = 0;
                isDead = false;
                armGraphics.SetActive(true);
                armAnim = 1;
                animator.SetBool("Muerto", false);
                animator.SetBool("Resucitar", true);
                characterGraphics.transform.localScale = new Vector3(1, 1, 1);
                followTarget.FollowY();
            }
        }
    }

    public void playerAnimation()
    {
        if (IsJumping == false)
        {
            Debug.Log("Changing animation to Idle");
            if (armAnim != 3)
            {
                armAnim = 1;
            }
            armAnimator.SetBool("Saltando", false);
            animator.SetBool("Saltando", false);
        }

        if (IsJumping == true)
        {

            Debug.Log("Changing animation to Jumping");
            if (armAnim == 1)
            {
                soundJump.Play();
                armAnim = 2;
                armAnimator.SetBool("Saltando", true);
                animator.SetBool("Saltando", true);
            }
            else if (armAnim == 3)
            {
                armAnimator.SetBool("Disparando", true);
                animator.SetBool("Saltando", true);
            }

        }

        if (IsShooting == true)
        {
            Debug.Log("Changing animation to Shooting");
            soundShoot.Play();
            armAnim = 3;
            armAnimator.SetBool("Disparando", true);
        }
    
        if (armAnim == 1)
        {
            Debug.Log("Animacion activa: Normal");

            armGraphicsShoot.SetActive(false);
            graphics.SetActive(true);

            if (PlayerDirection == 1)
            {
                graphics.transform.localScale = new Vector3(-1, 1, 1);
            }

            if (PlayerDirection == 2)
            {
                graphics.transform.localScale = new Vector3(1, 1, 1);
            }

            if (move > 0 && scalaActual > 0)
            {
                PlayerDirection = 1;
            }
            else if (move < 0 && scalaActual < 0)
            {
                PlayerDirection = 2;
            }
        }
        
        if (armAnim == 2)
        {
            Debug.Log("Animacion activa: Saltando");

            //armGraphics.SetActive(false);
            //armGraphicsJump.SetActive(true);

            armGraphicsShoot.SetActive(false);
            graphics.SetActive(true);

            if (PlayerDirection == 1)
            {
                graphics.transform.localScale = new Vector3(-1, 1, 1);
            }

            if (PlayerDirection == 2)
            {
                graphics.transform.localScale = new Vector3(1, 1, 1);
            }

            if (move > 0 && scalaActual > 0)
            {
                PlayerDirection = 1;
            }
            else if (move < 0 && scalaActual < 0)
            {
                PlayerDirection = 2;
            }
        }

        if (armAnim == 3)
        {
            Debug.Log("Animacion activa: Disparo");
            
            if (shootReady == true)
            {
                /*
                mivector = new Vector3(Input.mousePosition.x, Input.mousePosition.y, Camera.main.nearClipPlane);
                lookAt = Camera.main.ScreenToWorldPoint(mivector);

                AngleRad = Mathf.Atan2(lookAt.y - armGraphicsShoot.transform.position.y, lookAt.x - armGraphicsShoot.transform.position.x);

                AngleDeg = (180 / Mathf.PI) * AngleRad;

                armGraphicsShoot.transform.rotation = Quaternion.Euler(0, 0, AngleDeg);
                */
                
                if (PlayerDirection == 1)
                {
                    graphics.transform.localScale = new Vector3(-1, 1, 1);
                    armGraphics.transform.localScale = new Vector3(-1, 1, 1);
                    armGraphicsShoot.transform.localPosition = new Vector3(-0.029f, 0.202f, -1);
                }

                if (PlayerDirection == 2)
                {
                    graphics.transform.localScale = new Vector3(1, 1, 1);
                    armGraphics.transform.localScale = new Vector3(1, 1, 1);
                    armGraphicsShoot.transform.localPosition = new Vector3(0.052f, 0.202f, -1);
                }

                if (move > 0 && scalaActual > 0)
                {
                    PlayerDirection = 1;
                }
                else if (move < 0 && scalaActual < 0)
                {
                    PlayerDirection = 2;
                }

                Debug.Log("Dispara");
                armGraphics.SetActive(false);
                armGraphicsShoot.SetActive(true);
                //Instantiate(bulletGraphics, transform.position, transform.rotation);
                Instantiate(bulletGraphics, armGraphicsShoot.transform.position, armGraphicsShoot.transform.rotation);
                shootReady = false;
            }

        }

        // Animations: 1. Idle 2. Jumping 3. Shooting
    }

    public void DirectonIndicator(int value)
    {
        if (PlayerDirection == 1)
        {
            value = 1;
        }else if (PlayerDirection == 2)
        {
            value = 2;
        }
    }

    public void playerCamera()
    {
        Debug.Log("Asignando camara al jugador");
        followTarget = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<FollowTarget>();
    }

    public void playerRigidBody()
    {
        rb2d = GetComponent<Rigidbody2D>();
        graphics.transform.localScale = new Vector3(-1, 1, 1);
    }

    public void playerPhysics()
    {
        if (IsDead) { return; } // Si se esta muerto, sal de la función.

        //rb2d.velocity = new Vector2(move * maxS, rb2d.velocity.y);
        rb2d.AddForce(Vector2.right * move * maxS);

        animator.SetFloat("velocidadX", Math.Abs(rb2d.velocity.x));
        armAnimator.SetFloat("velocidadX", Math.Abs(rb2d.velocity.x));

    }

    public void playerActions()
    {
        if (!stopMovement)
        {
            move = Input.GetAxis("Horizontal");
            jump = Input.GetKeyDown(KeyCode.Space);
            attack = Input.GetButton("Fire1");
        }
        else
        {
            move = 0;
            followTarget.FollowY();
        }

        if (attack && isGrounded)
        {
            move = 0f;
            jump = false;
            armAnimator.SetBool("Disparando", true);
        }
        else
        {
            armAnimator.SetBool("Disparando", false);
        }

        if (jump && isGrounded)
        {
            Debug.Log("Salta");
            jump = false;
            IsJumping = true;
            rb2d.AddForce(Vector2.up * fuerzaSalto, ForceMode2D.Impulse);
        }

        if (Input.GetButton("Fire1"))
        {
            Debug.Log("Dispara");
            armAnim = 3;
        }

        scalaActual = graphics.transform.localScale.x;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            isGrounded = true;
            IsJumping = false;
        }

        if (collision.tag == "Trampoline")
        {
            rb2d.AddForce(Vector2.up * fuerzaSalto * 3, ForceMode2D.Impulse);
            IsJumping = true;
            animator.SetBool("Saltando", true);
            armAnimator.SetBool("Saltando", true);
        }

        if (collision.tag == "Fall")
        {
            Debug.Log("Fall");
            followTarget.FollowY();
        }

        if (collision.tag == "GroundFall")
        {
            Debug.Log("You have felt");
            followTarget.StopFollowY();
            isGrounded = true;
            IsJumping = false;
            animator.SetBool("Saltando", false);
            armAnimator.SetBool("Saltando", false);
        }


        if (collision.tag == "Finish")
        {
            Debug.Log("You win");

            SceneManager.LoadScene("GameOver");
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrounded = false;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("Colisión");
        if (collision.gameObject.tag == "Mortal" || collision.gameObject.tag == "Enemy")
        {
            sonidomuerte();
            Debug.Log("Muerte");
            animator.SetBool("Muerto", true);
            animator.SetBool("Resucitar", false);
            
            characterGraphics.transform.localScale = new Vector3(-0.25f, 0.25f, 0.25f);

            isDead = true;
            armGraphics.SetActive(false);
            armGraphicsShoot.SetActive(false);
        }
    }

    public void sonidomuerte()
    {
        soundHit.Play();
    }

    public void PulsaBacktoMenu()
    {
        Debug.LogError("He pulsado Back to Menu");

        SceneManager.LoadScene("MainMenu");
    }
}