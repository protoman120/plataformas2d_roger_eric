﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowTarget : MonoBehaviour
{
    public Transform TargetToFollow;
    bool HasToFollowY = false;

    // Update is called once per frame

    void Update()
    {
        if (HasToFollowY == false){
            this.gameObject.transform.position = new Vector3(TargetToFollow.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z);
        }else if (HasToFollowY == true)
        {
            this.gameObject.transform.position = new Vector3(TargetToFollow.position.x, TargetToFollow.position.y, this.gameObject.transform.position.z);
        }
    }

    public void FollowY()
    {
        Debug.Log("FollowY");
        HasToFollowY = true;
    }

    public void StopFollowY()
    {
        Debug.Log("StopFollowY");
        HasToFollowY = false;
    }
}
