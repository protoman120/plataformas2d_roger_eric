﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public int EnemyType;
    private int EnemyLife;
    [SerializeField] BoxCollider2D EnemyCollider;
    [SerializeField] GameObject EnemyGraphics;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Assigning Enemy Life");
        //Enemy Types: 1. BolaConPatas
        if (EnemyType == 1)
        {
            Debug.Log("Life: BolaConPatas Loaded");
            EnemyLife = 3;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (EnemyLife <= 0)
        {
            Debug.Log("Enemy is defeated");
            EnemyCollider.enabled = false;
            EnemyGraphics.SetActive(false);
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bullet")
        {
            Debug.Log("Enemy Damage Taken");
            EnemyLife--;
        }
    }
}
