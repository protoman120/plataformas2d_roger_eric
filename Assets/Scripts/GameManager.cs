﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public MyController2 PlayerController;
    [SerializeField] GameObject MusicBackground;


    // Start is called before the first frame update
    void Start()
    {
        PlayerController.playerCamera();
    }

    private void Awake()
    {
        PlayerController.playerRigidBody();

        if (PlayerPrefs.GetInt("music", 1) == 1)
        {
            MusicBackground.SetActive(true);
        }
        else
        {
            MusicBackground.SetActive(false);
        }

    }


    private void FixedUpdate()
    {
        PlayerController.playerPhysics();
    }
    // Update is called once per frame
    void Update()
    {
        PlayerController.playerActions();
        PlayerController.playerAnimation();
    }
}
