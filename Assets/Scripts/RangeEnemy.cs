﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeEnemy : MonoBehaviour
{

    [SerializeField] float rangeDistance;
    [SerializeField] Transform player;
    [SerializeField] public int velocidadEnemigo;
    [SerializeField] Patrullero patrol;

    // Update is called once per frame
    void Update()
    {
        if (Mathf.Abs(Vector2.Distance(player.position, transform.position)) < rangeDistance)
        {
            patrol.enabled = false;
            Debug.Log("Player in range of enemy");
            transform.position = Vector2.MoveTowards(transform.position, player.position, Time.deltaTime * velocidadEnemigo);
        }
        else
        {
            patrol.enabled = true;
        }
    }

    private void DrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, 0.5f);
    }
}
