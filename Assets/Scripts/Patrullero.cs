﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrullero : MonoBehaviour
{
    [SerializeField] Transform[] posiciones;
    [SerializeField] int posactual = 0;
    [SerializeField] float velocidadEnemigo = 1.0f;
    [SerializeField] GameObject EnemyGraphics;
    private int direction = 1;
    //Direction 1. Left / 2. Right
    public int EnemyType;
    //Types: 1. BolaConPatas 2.Enemy1 3.Enemy2 4.Gorilla

    private void Update()
    {
        //Debug.Log("Distancia: " + Vector2.Distance(posiciones[posactual].position, transform.position));
        if (Mathf.Abs(Vector2.Distance(posiciones[posactual].position, transform.position)) < 0.1f)
        {
            posactual++;
            
            if (direction == 1)
            {
                direction = 2;
            }
            else if (direction == 2)
            {
                direction = 1;
            }

            if (posactual >= posiciones.Length)
            {
                posactual = 0;
            }
        }

        if (direction == 1)
        {
            if (EnemyType == 1)
            {
                EnemyGraphics.transform.localScale = new Vector3(3, 3, 3);
            }
            
            if (EnemyType == 2)
            {
                EnemyGraphics.transform.localScale = new Vector3(1, 1, 1);
            }
            
            if (EnemyType == 3)
            {
                EnemyGraphics.transform.localScale = new Vector3(1, 1, 1);
            }
            
            if (EnemyType == 4)
            {
                EnemyGraphics.transform.localScale = new Vector3(2.5f, 2.5f, 2.5f);
            }
        }
        
        if (direction == 2)
        {
            if (EnemyType == 1)
            {
                EnemyGraphics.transform.localScale = new Vector3(-3, 3, 3);
            }
            
            if (EnemyType == 2)
            {
                EnemyGraphics.transform.localScale = new Vector3(-1, 1, 1);
            }
            
            if (EnemyType == 3)
            {
                EnemyGraphics.transform.localScale = new Vector3(-1, 1, 1);
            }
            
            if (EnemyType == 4)
            {
                EnemyGraphics.transform.localScale = new Vector3(-2.5f, 2.5f, 2.5f);
            }
        }

        transform.position = Vector2.MoveTowards(transform.position, posiciones[posactual].position, Time.deltaTime * velocidadEnemigo);
    }
}
