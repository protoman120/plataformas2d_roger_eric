﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{

    public float speed;
    public GameObject ShootSprite;
    public BoxCollider2D BulletCollider;
    private MyController2 mycontroller;
    [SerializeField] AudioSource HitSound;
    private int direction;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime, 0, 0);

        mycontroller.DirectonIndicator(direction);

        if (direction == 1)
        {
            ShootSprite.transform.localScale = new Vector3(-1, 1, 1);
        }

        if (direction == 2)
        {
            ShootSprite.transform.localScale = new Vector3(1, 1, 1);
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Finish" || other.tag == "Enemy")
        {
            StartCoroutine(DestroyBullet());
        }
    }

    IEnumerator DestroyBullet()
    {
        BulletCollider.enabled = false;

        yield return new WaitForSeconds(0.1f);

        Destroy(gameObject);
    }

}
